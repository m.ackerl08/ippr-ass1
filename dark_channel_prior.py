# Do not import any other modules
import imageio
import matplotlib.pyplot as plt
import numpy as np
from guided_filtering import guided_image_filtering
# You may use **one** filter of the following module to implement the
# ```dark_channel``` function
from scipy.ndimage import minimum_filter


def dark_channel(im, patch_size):
    '''
    Extract the "dark channel" from the image `im`, using a patch size
    of `patch_size` pixels (Eq. 7).
    '''
    print("computing dark channel")
    '''reach = patch_size//2
    x_size = im.shape[0]
    y_size = im.shape[1]
    im_min_rgb = np.zeros(im.shape)
    

    for c in range(im.shape[2]):
        for x in range(x_size):
            for y in range(y_size):
                im_min_rgb[x, y, c] = np.amin(im[max(0, x - reach) : min(x_size, x + reach), 
                                              max(0, y - reach):min(y_size, y + reach),
                                              c])
    im_dark = np.amin(im_min_rgb, axis=2)'''
    
    # im_min_rgb = np.zeros(im.shape)
    # for c in range(im.shape[2]):
    #     im_min_rgb[:, :, c] = minimum_filter(im[:,:,c], size=(patch_size,patch_size))

    # im_dark = np.amin(im_min_rgb, axis=2)

   # print("shape im", im.shape)
   # print("shape im_dark", im_dark.shape)
    
    # return im_dark
    return np.amin(minimum_filter(im[:,:,:], size=(patch_size,patch_size,3)), axis=2)


def atmosphere_color(im, dark):
    '''
    Get the 0.1% brightest pixels in the dark channel, and get the color of the
    atmosphere from the brightest pixels at these locations in the original
    image.
    '''
    print("computing atmosphere")
    point1_number = dark.size // 1000    
    n = dark.size - point1_number
    point1_brightest_flat_indices = np.argpartition(dark.ravel(), n)[n:] 
    point1_row_idx, point1_col_idx = np.unravel_index(point1_brightest_flat_indices, dark.shape)
    brightest = im[point1_row_idx, point1_col_idx,:]

    #print("brightests", brightest.shape)
    atmosphere = np.amax(brightest, axis=0)
    #print("atmosphere", atmosphere.shape)
    return atmosphere


def transmission(im, atm, patch_size, omega=0.95):
    '''
    Calculate the transmission map according to Eq. 11.
    '''
    print("computing transmission")
    '''reach = patch_size//2
    x_size = im.shape[0]
    y_size = im.shape[1]'''
    im_min_rgb = np.zeros(im.shape)
    

    for c in range(im.shape[2]):
        '''for x in range(x_size):
            for y in range(y_size):
                im_min_rgb[x, y, c] = np.amin(im[max(0, x - reach) : min(x_size, x + reach), 
                                              max(0, y - reach):min(y_size, y + reach),
                                              c]) / atm[c]'''
        im_min_rgb[:, :, c] = minimum_filter(im[:,:,c], size=(patch_size,patch_size)) / atm[c]
    im_dark = 1 - omega *np.amin(im_min_rgb, axis=2)
    

    return im_dark


def refine_transmission(im, t, r=20, eps=0.001):
    '''nr
    Refine the transmission estimate using the guided image filter.
    '''
    print("refining transmission")
    print(im.shape, t.shape)
    t_refined = np.zeros_like(t)
    t_refined = guided_image_filtering(t, np.mean(im, axis=2), r, eps)
    return t_refined
    #return t


def restore(im, t, atm, t_0=0.1):
    '''
    Estimate the scene radiance (Eq. 12).
    '''
    print("restoring radiance")
    im_restore = np.zeros_like(im)
    for x in range(im.shape[0]):
        for y in range(im.shape[1]):
            im_restore[x,y] = ((im[x,y] - atm) / max(t[x,y], t_0)) + atm
    return im_restore


if __name__ == '__main__':
    input = imageio.imread('./imgs/dehazing/input.png') / 255.

    patch_size = 15

    dark = dark_channel(input, patch_size)
    atmosphere = atmosphere_color(input, dark)
    t_estimate = transmission(input, atmosphere, patch_size)
    t_refined = refine_transmission(input, t_estimate)
    restored = restore(input, t_refined, atmosphere, t_0=0.1)
    restored_unrefined = restore(input, t_estimate, atmosphere, t_0=0.1)

    imageio.imsave('./imgs/dehazing/dark_channel.png', np.clip(dark, 0, 1))
    imageio.imsave('./imgs/dehazing/restored.png', np.clip(restored, 0, 1))
    imageio.imsave('./imgs/dehazing/restored_unrefined.png', np.clip(restored_unrefined, 0, 1))
    imageio.imsave('./imgs/dehazing/te.png', np.clip(t_estimate, 0, 1))
    imageio.imsave('./imgs/dehazing/t.png', np.clip(t_refined, 0, 1))
