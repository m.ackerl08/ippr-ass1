# Do not import any other modules
import numpy as np
import imageio
# You may use **one** filter of the following module to implement the
# ```compute_mean``` function
from scipy.ndimage.filters import uniform_filter


def compute_mean(image, filter_size):
    '''
    For each pixel in the image image consider a window of size
    filter_size x filter_size around the pixel and compute the mean
    of this window.
    @return: image containing the mean for each pixel
    '''

    print("Compute mean\n")
    mean_image = np.zeros_like(image)

    print(mean_image.shape)
    mean_image = uniform_filter(image, filter_size)

    return mean_image


def compute_variance(image, filter_size):
    '''
    For each pixel in the image image consider a window of size
    filter_size x filter_size around the pixel and compute the variance
    of this window.
    @return: image containing the variance (\\sigma^2) for each pixel
    '''

    print("Compute variance\n")
    
    mean_image = compute_mean(image, filter_size)
    sqr_mean_image = compute_mean(image**2, filter_size)

    return sqr_mean_image - mean_image**2


def compute_a(I, p, mu, p_bar, variance, eps, filter_size):
    '''
    Compute the intermediate result 'a' as described in the task in Eq. 3.
    @return: image containing a_k for each pixel
    '''
    print("Compute a\n")
    return_image = np.zeros_like(I)
   
    reach = filter_size//2
    x_max = I.shape[0]
    y_max = I.shape[1]

    cardinality_inverse = 1.0 / np.power(filter_size, 2)
    I_p = I*p  # calculate before to save time
    mu_pbar = mu*p_bar

    for x in range(x_max):
        for y in range(y_max):
            # I_p_patch = I_p[max(0, x - reach):min(x_max, x + reach),
            #                 max(0, y - reach):min(y_max, y + reach)]

            min_y = y - filter_size//2
            max_y = y + filter_size//2
            min_x = x - filter_size//2
            max_x = x + filter_size//2
            I_p_patch = np.zeros((filter_size,filter_size))
            x_idx = 0
            y_idx = 0

            for l in range(min_y, max_y+1):
                x_idx = 0
                for m in range(min_x, max_x+1):
                    if l < y_max and m < x_max:
                        if l>=0 and m>=0:
                            I_p_patch[y_idx,x_idx] = I_p[m,l]
                        elif l<0 and m>=0:
                            I_p_patch[y_idx,x_idx] = I_p[m,abs(l+1)]
                        elif l>=0 and m<0:
                            I_p_patch[y_idx,x_idx] = I_p[abs(m+1),l]
                        elif l<0 and m<0:
                            I_p_patch[y_idx,x_idx] = I_p[abs(m+1),abs(l+1)]
                    elif l >= y_max and m < x_max:
                        if m<0:
                            I_p_patch[y_idx,x_idx] = I_p[abs(m+1),y_max-(l-y_max+1)]
                        else:
                            I_p_patch[y_idx,x_idx] = I_p[m,y_max-(l-y_max+1)]
                    elif l < y_max and m >= x_max:
                        if y<0:
                            I_p_patch[y_idx,x_idx] = I_p[x_max-(m-x_max+1),abs(l+1)]
                        else:
                            I_p_patch[y_idx,x_idx] = I_p[x_max-(m-x_max+1),l]
                    elif l >= y_max and m >= x_max:
                        I_p_patch[y_idx,x_idx] = I_p[x_max-(m-x_max+1),y_max-(l-y_max+1)]

                    x_idx += 1

                y_idx += 1
        
            return_image[x,y] = cardinality_inverse * np.sum(I_p_patch - mu_pbar[x,y]) / (variance[x,y] + eps)   

    print(return_image.shape)
    return return_image

def compute_b(p_bar, a, mu):
    '''
    Compute the intermediate result 'b' as described in the task in Eq. 4.
    @return: image containing b_k for each pixel
    '''

    print("Compute b\n")
    return_image = np.zeros_like(p_bar)
    return_image = p_bar - a*mu

    return return_image


def compute_q(a_bar, b_bar, I):
    '''
    Compute the final filtered result 'q' as described in the task (Eq. 5).
    @return: filtered image
    '''

    print("Compute q\n")
    q = np.zeros_like(a_bar)
    q = a_bar*I + b_bar

    return q


def fnf_denoising(image_dark, image_flash, r, eps):
    '''
    Call your guided image filtering method with the right parameters to
    perform flash-no-flash denoising.
    '''
    for channel in range(image_dark.shape[2]):
        image_dark[:,:,channel] = guided_image_filtering(image_dark[:,:,channel], image_flash[:,:,channel], r, eps)
    return image_dark


def detail_enhancement(inp, c=1, r=1, eps=1):
    '''
    Call your guided image filtering method with the right parameters and
    implement Eq. 13 of the assignment sheet.
    '''
    result = np.zeros_like(inp)
    q = np.zeros_like(inp)
    for channel in range(inp.shape[2]):
        q[:,:,channel] = guided_image_filtering(inp[:,:,channel], inp[:,:,channel], r, eps)
    
    result = c*(inp - q) + q

    return result


def guided_image_filtering(p, I, r=1, eps=1):
    filter_size = 2 * r + 1
    p_bar = compute_mean(p, filter_size)
    mu = compute_mean(I, filter_size)
    variance = compute_variance(I, filter_size)
    a = compute_a(p, I, p_bar, mu, variance, eps, filter_size)
    b = compute_b(p_bar, a, mu)
    a_bar, b_bar = compute_mean(a, filter_size), compute_mean(b, filter_size)
    return compute_q(a_bar, b_bar, I)


if __name__ == "__main__":
    r = 20
    eps = 0.01

    do_edge_pres_filtering = 0
    do_detail_enhancement = 1
    do_ftf = 0
    do_detail_smartphone = 0

    if do_detail_enhancement:
        input = imageio.imread('./imgs/detail_enhancement/input.png') / 255.
        c = .5
        out = detail_enhancement(input, c, r, eps)
        imageio.imwrite(
            f'./imgs/detail_enhancement/out_c_{c}r_{r}_eps_{eps:.4f}.png',
            out
        )
        c = 1.5
        out = detail_enhancement(input, c, r, eps)
        imageio.imwrite(
            f'./imgs/detail_enhancement/out_c_{c}r_{r}_eps_{eps:.4f}.png',
            out
        )
        c = 3
        out = detail_enhancement(input, c, r, eps)
        imageio.imwrite(
            f'./imgs/detail_enhancement/out_c_{c}r_{r}_eps_{eps:.4f}.png',
            out
        )
    if do_edge_pres_filtering:
        input = imageio.imread('./imgs/edge_pres_filtering/input.png') / 255.
        out = np.zeros_like(input) 
        for channel in range(input.shape[2]):
            out[:,:,channel] = guided_image_filtering(input[:,:,channel], input[:,:,channel], r, eps)
        imageio.imwrite(
            f'./imgs/edge_pres_filtering/out_r_{r}_eps_{eps:.4f}.png',
            out
        )
    if do_detail_smartphone:
        input = imageio.imread('./imgs/ftf/guide.png') / 255.
        out = detail_enhancement(input, 1.5, r, eps)
        imageio.imwrite(
            f'./imgs/ftf/out_detail_c_{1.5}_r_{r}_eps_{eps:.4f}.png',
            out
        )
        out2 = detail_enhancement(input, 3, r, eps)
        imageio.imwrite(
            f'./imgs/ftf/out_detail_c_{3}_r_{r}_eps_{eps:.4f}.png',
            out2
        )
        out3 = detail_enhancement(input, 4, r, eps)
        imageio.imwrite(
            f'./imgs/ftf/out_detail_c_{4}_r_{r}_eps_{eps:.4f}.png',
            out3
        )
    if do_ftf:
        input = imageio.imread('./imgs/ftf/input.png') / 255.
        guide = imageio.imread('./imgs/ftf/guide.png') / 255.
        out = fnf_denoising(input, guide, r, eps)
        imageio.imwrite(
            f'./imgs/ftf/out_r_{r}_eps_{eps:.4f}.png',
            out
        )
